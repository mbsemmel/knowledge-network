# Knowledge Network

This is a project for the lecture model based software development of the TU Braunschweig.

# Setup IDE

1. Checkout the repository to your favourite location.
2. To setup an IDE with all the required dependencies, use the 64 Bit Eclipse Installer (https://wiki.eclipse.org/Eclipse_Installer).
3. In the installer, switch to advanced mode and add the ide.setup from the checkout.
4. Under 'Variables' select a location to install eclipse to.
5. Enjoy 😍😍😍

## Things to generate

- mbsemmel.knowledge/Knowledge.genmodel
- mbsemmel.knowledge.lang/src/de.mbsemmel.knowledge.lang/GenerateKnow.mwe2
- mbsemmel.calendar/Calendar.genmodel