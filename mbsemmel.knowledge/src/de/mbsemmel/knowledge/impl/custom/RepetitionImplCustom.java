package de.mbsemmel.knowledge.impl.custom;

import de.mbsemmel.knowledge.impl.RepetitionImpl;

public class RepetitionImplCustom extends RepetitionImpl {

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (obj == null) {
			return false;
		}

		RepetitionImplCustom repetition = (RepetitionImplCustom) obj;

		return this.period == repetition.period && this.count == repetition.count;
	}

	@Override
	public int hashCode() {
		if (period != null) {
			return Integer.hashCode(count) ^ Integer.rotateLeft(period.hashCode(), 3);
		} else {
			return Integer.hashCode(count);
		}
	}

}
