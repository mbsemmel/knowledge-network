package de.mbsemmel.knowledge.impl.custom;

import de.mbsemmel.knowledge.TimeSpec;
import de.mbsemmel.knowledge.impl.TimeSpecImpl;

public class TimeSpecImplCustom extends TimeSpecImpl {

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (obj == null) {
			return false;
		}

		if (!(obj instanceof TimeSpec)) {
			return false;
		}
		TimeSpecImplCustom object = (TimeSpecImplCustom) obj;

		return this.start == object.start && this.end == object.end && this.repetition == object.repetition;
	}

	@Override
	public int hashCode() {
		if (repetition != null) {
			return start.hashCode() ^ Integer.rotateLeft(end.hashCode(), 3)
					^ Integer.rotateLeft(repetition.hashCode(), 6);
		} else {
			return start.hashCode() ^ Integer.rotateLeft(end.hashCode(), 3);
		}
	}

	public boolean isStartBeforeEnd() {
		return start.isBefore(end);
	}

}
