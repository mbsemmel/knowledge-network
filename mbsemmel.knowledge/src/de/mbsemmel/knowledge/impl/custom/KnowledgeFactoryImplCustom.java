package de.mbsemmel.knowledge.impl.custom;

import java.time.Duration;
import java.time.Period;
import java.time.ZonedDateTime;

import org.eclipse.emf.ecore.EDataType;

import de.mbsemmel.knowledge.Repetition;
import de.mbsemmel.knowledge.TimeSpec;
import de.mbsemmel.knowledge.impl.KnowledgeFactoryImpl;

public class KnowledgeFactoryImplCustom extends KnowledgeFactoryImpl {
	@Override
	public Duration createDurationFromString(EDataType eDataType, String initialValue) {
		return Duration.parse(initialValue);
	}

	@Override
	public ZonedDateTime createZonedDateTimeFromString(EDataType eDataType, String initialValue) {
		return ZonedDateTime.parse(initialValue);
	}

	@Override
	public Period createPeriodFromString(EDataType eDataType, String initialValue) {
		return Period.parse(initialValue);
	}

	@Override
	public TimeSpec createTimeSpec() {
		TimeSpecImplCustom timeSpec = new TimeSpecImplCustom(); 	
		return timeSpec;
	}

	@Override
	public Repetition createRepetition() {
		RepetitionImplCustom repetition = new RepetitionImplCustom();
		return repetition;
	}

}
