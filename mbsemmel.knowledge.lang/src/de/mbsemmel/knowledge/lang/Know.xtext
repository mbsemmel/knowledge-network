grammar de.mbsemmel.knowledge.lang.Know with org.eclipse.xtext.common.Terminals

import "https://mbsemmel.de/knowledge"
import "http://www.eclipse.org/emf/2002/Ecore" as ecore

Knowledge returns Knowledge:
	{Knowledge}
	(('info' informations+=Information) |
	('event' events+=Event) |
	('task' tasks+=Task) |
	('res' resources+=Resource) |
	('tag' tags+=Tag) |
	('resType' resourceTypes+=Tag))*;

Information returns Information:
	name=ID
	(description=DESC_STRING)? &
	('created' created=ZonedDateTime)? &
	("#" tags+=[Tag|ID])* &
	("~" associations+=[Information|ID])*;

Event returns Event:
	name=ID
	(description=DESC_STRING)? &
	('created' created=ZonedDateTime)? &
	('$' priority=EInt)? &
	('!' reminder+=Duration)* &
	("#" tags+=[Tag|ID])* &
	("~" associations+=[Information|ID])* &
	("+" resources+=[Resource|ID])* &
	("@" timeSpec+=TimeSpec)*;

Task returns Task:
	name=ID
	(description=DESC_STRING)? &
	('created' created=ZonedDateTime)? &
	('$' priority=EInt)? &
	('%' progress=EDouble)? &
	('^' estimatedEffort=Duration)? &
	('!' reminder+=Duration)* &
	('#' tags+=[Tag|ID])* &
	('~' associations+=[Information|ID])* &
	('+' resources+=[Resource|ID])* &
	('-' subTasks+=[Task|ID])* &
	('<' precedingTasks+=[Task|ID])* &
	('@' timeSpec+=TimeSpec)*;

Resource returns Resource:
	{Resource}
	name=ID
	(description=DESC_STRING)?
	('=' types+=[Tag|ID] ("=" types+=[Tag|ID])*)?;

Tag returns Tag:
	{Tag}
	name=ID
	(description=DESC_STRING)?;

TimeSpec returns TimeSpec:
	start=ZonedDateTime "-" end=ZonedDateTime
	(repetition=Repetition)?;

Repetition returns Repetition:
	{Repetition}
	(count=EInt "*")?
	(period=Period);

EDouble returns ecore::EDouble:
	INT? '.' INT (('E' | 'e') '-'? INT)?;

EInt returns ecore::EInt:
	INT;

Period returns Period:
	(EInt 'Y') | (EInt 'M') | (EInt 'W') | (EInt 'D');

Duration returns Duration:
	(EInt 'D')? ('T' (EInt ('H' | 'M' | 'S'))+)?;

ZonedDateTime returns ZonedDateTime:
	ISO8601_TIMESTRING;

terminal ISO8601_TIMESTRING:
	('0'..'9') ('0'..'9') ('0'..'9') ('0'..'9') '-' ('0'..'1') ('0'..'9') '-' ('0'..'3') ('0'..'9')
	'T' ('0'..'2') ('0'..'9') ':' ('0'..'5') ('0'..'9')
	(('+' | '-') ('0'..'2') ('0'..'9') ':' ('0'..'5') ('0'..'9') | 'Z')?;

terminal DESC_STRING:
	'?' !('\n' | '\r')* ('\r'? '\n')?;
