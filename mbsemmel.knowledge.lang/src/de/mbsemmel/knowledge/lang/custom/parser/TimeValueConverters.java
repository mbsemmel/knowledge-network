package de.mbsemmel.knowledge.lang.custom.parser;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeParseException;

import org.eclipse.xtext.common.services.DefaultTerminalConverters;
import org.eclipse.xtext.conversion.IValueConverter;
import org.eclipse.xtext.conversion.ValueConverter;
import org.eclipse.xtext.conversion.ValueConverterException;
import org.eclipse.xtext.conversion.impl.AbstractToStringConverter;
import org.eclipse.xtext.nodemodel.INode;

public class TimeValueConverters extends DefaultTerminalConverters {
	@ValueConverter(rule = "ZonedDateTime")
	public IValueConverter<ZonedDateTime> ZonedDateTime() {
		return new AbstractToStringConverter<ZonedDateTime>() {

			@Override
			protected ZonedDateTime internalToValue(String string, INode node) throws ValueConverterException {
				try {
					return ZonedDateTime.parse(string);
				} catch (DateTimeParseException e) {
					return ZonedDateTime.of(LocalDateTime.parse(string), ZoneId.systemDefault());
				}
			}
		};
	}

	@ValueConverter(rule = "Period")
	public IValueConverter<Period> Period() {
		return new AbstractToStringConverter<Period>() {

			@Override
			protected java.time.Period internalToValue(String string, INode node) throws ValueConverterException {
				return Period.parse("P" + string);
			}
		};
	}

	@ValueConverter(rule = "Duration")
	public IValueConverter<Duration> Duration() {
		return new AbstractToStringConverter<Duration>() {

			@Override
			protected java.time.Duration internalToValue(String string, INode node) throws ValueConverterException {
				return Duration.parse("P" + string.replaceAll("\\s", ""));
			}
		};
	}
}
