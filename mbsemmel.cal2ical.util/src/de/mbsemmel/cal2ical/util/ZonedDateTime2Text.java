package de.mbsemmel.cal2ical.util;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneOffset;
import java.util.UUID;

public class ZonedDateTime2Text {
	public String getTimeZone(String time) {
		return ZonedDateTime.parse(time).getZone().toString();
	}
	
	public String getFormattedTime(String time) {
		return ZonedDateTime.parse(time).format(DateTimeFormatter.ofPattern("yyyyMMdd'T'HHmmss"));
	}
	
	
	public String getFormattedTimeNow() {
		return ZonedDateTime.now(ZoneOffset.UTC).format(DateTimeFormatter.ofPattern("yyyyMMdd'T'HHmmss'Z'"));
	}
	
	public String getUUID() {
		return UUID.randomUUID().toString().toUpperCase();
	}
}
