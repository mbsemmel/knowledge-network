package de.mbsemmel.m2m.scheduler;


import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.time.Duration;
import java.time.Instant;
import java.time.Period;
import java.time.ZoneId;
import java.util.*;

import de.mbsemmel.calendar.CalendarFactory;
import de.mbsemmel.calendar.Event;



public class Scheduler {
	/*
	 * Day Class
	 */
	private class Day{
		long start;
		long end;
		List<Time> timeList;
		
		public Day(long start, long end) {
			this.start = start;
			this.end = end;
			Time daytime = new Time(start, end, true);
			timeList = new ArrayList<Time>();
			timeList.add(daytime);
		}
		
		public Time addTask(long start, long end, long amount, String name) {
			boolean fit = false;
			Iterator<Time> it = timeList.iterator();
			List<Time> newL = new ArrayList<Time>();
			int i = 0;
			while (fit == false && it.hasNext()) {
				Time next = it.next();
				if (next.fits(start, end, amount)) {
					fit = true;
					newL = next.addTask(start, end, amount, name);
					timeList.remove(i);
					timeList.addAll(i, newL);
				}
				i++;
			}
			if (fit) {
				Time ret = null;
				for (Time t: newL) {
					if (t.name == name) {
						ret = t;
					}		
				}
				return ret;
			}
			return null;
		}
		
		@Override
		public String toString() {
			String s = "";
			for (int i = 0; i < timeList.size(); i++) {
				s = s + i + " Start = " + timeList.get(i).start + "   Ende = " + timeList.get(i).end + "  ";
				s = s + "Amount = " + (timeList.get(i).end-timeList.get(i).start) + "  ";
				if (timeList.get(i).isFree()) {
					s = s + "+";
				} else {
					s = s + "-";
				}
				s = s +"  " + timeList.get(i).name+ "\n";
			}
			return s;
		}
		
		public boolean addEvent(long start, long end) {
			//Put start + end inside day boundaries
			if (start > this.end) {
				return false;
			}
			if (end < this.start) {
				return false;
			}
			if (start < this.start) {
				start = this.start;
			}
			if (end > this.end) {
				end = this.end;
			}
			if (end-start <= 0) {
				return false;
			}
			List<Time> neu = new ArrayList<Time>();
			for (int i = 0; i < timeList.size(); i++) {
					if (timeList.get(i).end < start) {
						neu.add(timeList.get(i));
					} else {
						// End of neu < current position start 
						if (neu.size() == 0 || neu.get(neu.size()-1).end <= timeList.get(i).start) {
							// Event is between new and old
							if (timeList.get(i).start <= end) {
								Time tmp = timeList.get(i);
								if (!tmp.isFree()) {
									tmp.end = end;
									neu.add(tmp);
								} else {
									Time tmp2 = new Time(tmp.start, tmp.end, tmp.free);
									tmp.end = start;
									neu.add(tmp);
									Time addTime = new Time(start, end, false);
									neu.add(addTime);
									if (addTime.end < tmp2.end) {
										tmp2.start = addTime.end;
										neu.add(tmp2);
									}
								}
							}
						} else {
							// End of neu > current position start
							if (neu.size() == 0 || neu.get(neu.size()-1).end <= timeList.get(i).end) {
								Time addTime = new Time(neu.get(neu.size()-1).end, timeList.get(i).end, timeList.get(i).free);
								neu.add(addTime);
							}
						
					}
				}
			}
			timeList = neu;
			return true;
		}
	}
	/*
	 * Time Class
	 */
	private class Time{
		long start;
		long end;
		String name = "";
		boolean free;
		
		public Time(long start, long end, boolean free) {
			this.start = start;
			this.end = end;
			this.free = free;
		}
		
		public Time(long start, long end, boolean free, String name) {
			this.start = start;
			this.end = end;
			this.free = free;
			this.name = name;
		}
		
		public boolean isFree() {
			return free;
		}
		
		public String toString() {
			String s = "";
			s = s + " Start = " + this.start + "   Ende = " + this.end + "  ";
			s = s + "Amount = " + (this.end-this.start) + "  " + this.name+ "  ";
			if (this.free) {
				s = s + "+";
			} else {
				s = s + "-";
			}
			return s;
		}
		
		public boolean fits(long start, long end, long amount) {
			//Check if time is free time or blocking time
			if (isFree()) {
				//Check if time fits into timeslot
				if (amount > this.end - this.start) {
					return false;
				}
				if (start > this.start) {
					if (end < this.end) {
						return end-start>=amount;
					} else {
						return this.end-start>=amount;
					}
				} else {
					if (end < this.end) {
						return end-this.start>=amount;
					} else {
						return this.end-this.start>=amount;
					}
				}
			}
			return false;
		}
		
		
		private long getStart(long start) {
			//Check if time is free time or blocking time
			if (start < this.start) {
				return this.start;
			} else {
				return start;
			}
		}
		

		// Add an task to the list
		public List<Time> addTask(long start, long end, long amount, String name) {
			List<Time> newList = new ArrayList<Time>();
			if (fits(start, end, amount)) {
				Time newTime = new Time(getStart(start), getStart(start) + amount, false, name);
				if (getStart(start) == this.start) {
					newList.add(newTime);
					if (getStart(start) + amount == this.end) {
						return newList;
					} else {
						this.start = getStart(start) + amount;
						newList.add(this);
						return newList;
					}
				} else {
					if (this.end > getStart(start) + amount) {
						Time newfreeTime = new Time(getStart(start) + amount,this.end, true);
						this.end = start;
						newList.add(this);
						newList.add(newTime);
						newList.add(newfreeTime);
						return newList;
					} else {
						this.end = getStart(start);
						newList.add(this);
						newList.add(newTime);
						return newList;
					}
					
				}
			}
			return null;
			
		}
	}
	
	private List<Day> week = new ArrayList<Day>();

	
	public Scheduler() {
		ZonedDateTime now = ZonedDateTime.now();
		week.add(getDay(now));
		ZonedDateTime next = now.minusHours(now.getHour()).minusMinutes(now.getMinute());
		for (int i = 1; i < 30; i++) {
			week.add(getDay(next.plusDays(i) ));
		}
	}
	
	
	public Day getDay(ZonedDateTime start) {
		if (start.getHour()>=22) {
			start = start.plusDays(1);
			start = start.minusMinutes(start.getMinute());
			start = start.minusSeconds(start.getSecond());
			start = start.minusNanos(start.getNano());
			start = start.minusHours(start.getHour()).plusHours(8);
		} else {
			if (start.getHour()<8) {
				start = start.minusMinutes(start.getMinute());
				start = start.minusSeconds(start.getSecond());
				start = start.minusNanos(start.getNano());
				start = start.minusHours(start.getHour()).plusHours(8);
			} else {
				start = start.minusSeconds(start.getSecond());
				start = start.minusNanos(start.getNano());
			}
		}
		long starttime = start.toInstant().getEpochSecond();
		ZonedDateTime ende = start;
		ende = ende.minusMinutes(ende.getMinute());
		ende = ende.minusSeconds(ende.getSecond());
		ende = ende.minusNanos(ende.getNano());
		ende = ende.minusHours(ende.getHour()).plusHours(22);
		long endtime = ende.toInstant().getEpochSecond();
		Day ret = new Day(starttime,endtime);
		return ret;
	}
	
	
	public boolean addEvent(String startString, String endString, String periodString, int count) {
		ZonedDateTime start = ZonedDateTime.parse(startString);
		ZonedDateTime ende = ZonedDateTime.parse(endString);	
		try {
			int curcount = 0;
			boolean limited = false;
			Period period = Period.parse(periodString);
			if (count > 0) {
				curcount = count-1; 
				limited = true;
			}
			while(start.toEpochSecond() < week.get(week.size()-1).end && curcount >= 0) {
				if (limited) {
					curcount--;
				}
				for (Day day: week) {
					day.addEvent(start.toEpochSecond(), ende.toEpochSecond());
				}
				start = start.plus(period);
				ende = ende.plus(period); 
			}
			return true;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		for (Day day: week) {
			day.addEvent(start.toEpochSecond(), ende.toEpochSecond());
		}
		return true;
	}
	
	public long getAmountAverage(long amount, long days) {
		if (amount <= 0) {
			return 0;
		}
		if (days == 0) {
			days = 1;
		}
		long amountAverage = amount / days;
		if (amountAverage < (45*60)) {
			if (amount < (45*60)) {
				amountAverage = amount;
			} else {
				amountAverage = (45*60);
			}
		}
		return amountAverage;
	}
	
	public Collection<Event> addTask(String startString, String endString, String duration, String progressString, String name, String description) {
		double rProgress = 1;
		List<Event> ret = new ArrayList<Event>();
		if (duration == "") {
			return ret;
		}
		try {
			rProgress = 1-Double.parseDouble(progressString);
		} catch (Exception e) {
			rProgress = 1;
		}
		long durationValue = 0;
		try {
			durationValue = Duration.parse(duration).getSeconds();
		} catch (Exception e) {
			return ret;
		}
		
		long  amount = (long)(durationValue*rProgress);
		ZonedDateTime start = ZonedDateTime.parse(startString);
		ZonedDateTime ende = ZonedDateTime.parse(endString);
		long days = ChronoUnit.DAYS.between(start, ende);
		long amountAverage = getAmountAverage(amount, days);
		Time tmp = null;
		Event tmpEvent = CalendarFactory.eINSTANCE.createEvent();
		
		for (Day day: week) {
			if (amount > 0) {
				tmp = day.addTask(start.toEpochSecond(), ende.toEpochSecond(), amountAverage, name);
				if (tmp != null) {
					tmpEvent = CalendarFactory.eINSTANCE.createEvent();
					tmpEvent.setCreated(ZonedDateTime.now());
					tmpEvent.setName(name);
					tmpEvent.setDescription(description);
					tmpEvent.setStart(ZonedDateTime.ofInstant(Instant.ofEpochSecond(tmp.start), ZoneId.systemDefault()));
					tmpEvent.setEnd(ZonedDateTime.ofInstant(Instant.ofEpochSecond(tmp.end), ZoneId.systemDefault()));
					ret.add(tmpEvent);
					amount = amount - amountAverage;
					days = days -1;
					amountAverage = getAmountAverage(amount, days);
				}
			}
		}
		return ret;
	}
	
	
	public void printWeek() {
		int i = 0;
		for (Day day: week) {
			System.out.println("Day " + i++);
			System.out.println(day.toString());
		}
	}
}




