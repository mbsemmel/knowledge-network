/*******************************************************************************
 * Copyright (c) 2010, 2012 Obeo.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package de.mbsemmel.m2m.impl;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.m2m.atl.emftvm.EmftvmFactory;
import org.eclipse.m2m.atl.emftvm.ExecEnv;
import org.eclipse.m2m.atl.emftvm.Metamodel;
import org.eclipse.m2m.atl.emftvm.Model;
import org.eclipse.m2m.atl.emftvm.util.DefaultModuleResolver;
import org.eclipse.m2m.atl.emftvm.util.ModuleResolver;
import org.eclipse.m2m.atl.emftvm.util.TimingData;



/**
 * Entry point of the 'Knowledge2calendar' transformation module.
 */
public class Knowledge2calendar {

	
	public static void doModel2Model(Resource inputModel) {
		Knowledge2calendar runner = new Knowledge2calendar();
		runner.model2model(inputModel);
	}
		
	public static String pathComponent(String filename) {
		int i = filename.lastIndexOf("/");
		return (i > -1) ? filename.substring(0, i + 1) : filename;
	}
		  
	public static String filenameComponent(String filename) {
		int i = filename.lastIndexOf("/");
		String name = (i > -1) ? filename.substring(i + 1) : filename;
		i = name.lastIndexOf('.');
		return (i > -1) ? name.substring(0, i) : name;
	}
	
	
	public void model2model(Resource inModelResource) {
		XMIResourceFactoryImpl resFactory = new XMIResourceFactoryImpl() {};
		
		String path = pathComponent(inModelResource.getURI().toString());
		String filename = filenameComponent(inModelResource.getURI().toString());
		String pathModels = path+"models"+"/"+filename;
		URI xmiuri = URI.createURI(pathModels+".knowledge");
		
		XMIResource xmiresource = (XMIResource) resFactory.createResource(xmiuri);
		
		EObject xtextModel =  inModelResource.getAllContents().next();	
		xmiresource.getContents().add(xtextModel);
		
		try {
			xmiresource.save(new HashMap<String, Object>());
		} catch (IOException e) {
			e.printStackTrace();
		}
		

		// Load input metamodel and model
		ExecEnv envIn = EmftvmFactory.eINSTANCE.createExecEnv();
		ResourceSet rsIn = new ResourceSetImpl();

		// Load input metamodel
		Metamodel metaModelIn = EmftvmFactory.eINSTANCE.createMetamodel();
		metaModelIn.setResource(rsIn.getResource(URI.createURI("https://mbsemmel.de/knowledge"), true));
		envIn.registerMetaModel("Knowledge", metaModelIn);

		// Load input model
		Model inModel = EmftvmFactory.eINSTANCE.createModel();
		inModel.setResource(rsIn.getResource(xmiuri, true));
		envIn.registerInputModel("IN", inModel);
		
		// Load ouput metamodel and model	
		ResourceSet rsOut = new ResourceSetImpl();

		// Load output metamodel
		Metamodel metaModelOut = EmftvmFactory.eINSTANCE.createMetamodel();
		metaModelOut.setResource(rsOut.getResource(URI.createURI("https://mbsemmel.de/calendar"), true));
		envIn.registerMetaModel("Calendar", metaModelOut);

		// Load output model
		Model outModel = EmftvmFactory.eINSTANCE.createModel();
		outModel.setResource(rsOut.createResource(URI.createURI(pathModels+".calendar")));
		envIn.registerOutputModel("OUT", outModel);

		// Load and run module
		ModuleResolver mr = new DefaultModuleResolver("platform:/plugin/mbsemmel.m2m/src/de/mbsemmel/m2m/impl/", new ResourceSetImpl());
		TimingData td = new TimingData();
		envIn.loadModule(mr, "knowledge2calendar");
		td.finishLoading();
		envIn.run(td);
		td.finish();
	
		// Save models
		try {
			outModel.getResource().save(Collections.emptyMap());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
}
