package de.mbsemmel.m2t.commands;

import org.eclipse.core.commands.IHandler;

import de.mbsemmel.m2t.generator.ModelToTextGenerator;

public class GenerateICalHandler extends AbstractGenerateHandler implements IHandler {
	public GenerateICalHandler() {
		inputFileExtension = "calendar";
		outputFileExtension = "ical";
		generatorFunction = ModelToTextGenerator::generateICal;
	}

}
