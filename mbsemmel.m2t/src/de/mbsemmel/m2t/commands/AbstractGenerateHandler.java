package de.mbsemmel.m2t.commands;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.function.Function;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.PlatformUI;

public abstract class AbstractGenerateHandler extends AbstractHandler {

	protected String inputFileExtension;
	protected String outputFileExtension;
	protected Function<String, String> generatorFunction;

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IFile openFile = getOpenFile();
		String output = generatorFunction.apply(getInputPath(openFile));
		IFile outputFile = openFile.getParent().getParent().getFile(getOutputPath(openFile));
		InputStream source = new ByteArrayInputStream(output.getBytes());
		try {
			if (!outputFile.getParent().exists()) {
				IFolder folder = (IFolder) outputFile.getParent();
				folder.create(true, true, null);
			}
			if (!outputFile.exists()) {
				outputFile.create(source, true, null);
			} else {
				outputFile.setContents(source, true, true, null);
			}
		} catch (CoreException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public boolean isEnabled() {
		IFile openFile = getOpenFile();
		return openFile != null && "know".equals(openFile.getFileExtension());
	}

	private String getInputPath(IFile file) {
		return getRelatedPath(file, "models", inputFileExtension).toString();
	}

	private IPath getOutputPath(IFile file) {
		return getRelatedPath(file, "export", outputFileExtension);
	}

	private IPath getRelatedPath(IFile file, String folder, String extension) {
		IPath pathWithoutName = getPathWithoutName(file);
		String fileName = getFileName(file);
		IPath outputPath = pathWithoutName.append(folder).append(fileName);
		outputPath = outputPath.removeFileExtension().addFileExtension(extension);
		return outputPath;
	}

	private IPath getPathWithoutName(IFile file) {
		IPath path = file.getFullPath();
		return path.removeLastSegments(1);
	}

	private String getFileName(IFile openFile) {
		return openFile.getFullPath().lastSegment();
	}

	private IFile getOpenFile() {
		IEditorInput input = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor()
				.getEditorInput();
		if (input instanceof IFileEditorInput) {
			IFileEditorInput editorInput = (IFileEditorInput) input;
			return editorInput.getFile();
		}
		return null;
	}

}
