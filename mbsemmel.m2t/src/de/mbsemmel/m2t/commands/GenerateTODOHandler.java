package de.mbsemmel.m2t.commands;

import org.eclipse.core.commands.IHandler;

import de.mbsemmel.m2t.generator.ModelToTextGenerator;

public class GenerateTODOHandler extends AbstractGenerateHandler implements IHandler {

	public GenerateTODOHandler() {
		inputFileExtension = "knowledge";
		outputFileExtension = "todo.tex";
		generatorFunction = ModelToTextGenerator::generateToDo;
	}

}
