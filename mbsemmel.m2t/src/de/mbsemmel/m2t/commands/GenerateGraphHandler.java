package de.mbsemmel.m2t.commands;

import org.eclipse.core.commands.IHandler;

import de.mbsemmel.m2t.generator.ModelToTextGenerator;

public class GenerateGraphHandler extends AbstractGenerateHandler implements IHandler {

	public GenerateGraphHandler() {
		inputFileExtension = "knowledge";
		outputFileExtension = "graph.tex";
		generatorFunction = ModelToTextGenerator::generateGraph;
	}

}
