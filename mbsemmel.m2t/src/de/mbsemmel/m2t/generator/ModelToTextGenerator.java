package de.mbsemmel.m2t.generator;
import java.io.File;
import java.util.Map;

import org.eclipse.emf.common.CommonPlugin;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.epsilon.egl.EglFileGeneratingTemplateFactory;
import org.eclipse.epsilon.egl.EglTemplateFactoryModuleAdapter;
import org.eclipse.epsilon.emc.emf.InMemoryEmfModel;
import org.eclipse.epsilon.eol.IEolExecutableModule;

import de.mbsemmel.calendar.CalendarPackage;

public class ModelToTextGenerator {
	
	
	public static String exportModel(String eglPath, String modelXmiPath, String modelExtension) throws Exception {		    
		
		URI icaluri = URI.createFileURI(eglPath);
		String eglPathFull = CommonPlugin.resolve(icaluri).toFileString();
		
		//Parse Egl-File
		EglFileGeneratingTemplateFactory factory = new EglFileGeneratingTemplateFactory();
		IEolExecutableModule module = new EglTemplateFactoryModuleAdapter(factory);
		//factory.setOutputRoot("gen/event.ics");
		module.parse(new File(eglPathFull));
		
		//Load xmi-modell into emfmodel
		CalendarPackage.eINSTANCE.eClass();
        Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
        Map<String, Object> m = reg.getExtensionToFactoryMap();
        m.put(modelExtension, new XMIResourceFactoryImpl());

        // Obtain a new resource set
        ResourceSet resSet = new ResourceSetImpl();

        // create a resource
        Resource resource = resSet.getResource(URI.createURI(modelXmiPath), true);

		// Load the XML document
		InMemoryEmfModel model = new InMemoryEmfModel(resource);
		
		//Add model and execute model2text
		factory.getContext().getModelRepository().addModel(model);
		return (String)module.execute();
	}
	
	public static String generateGraph(String xmiPath) {
		try {
			return exportModel("platform:/plugin/mbsemmel.m2t/know2graph.egl", xmiPath, "knowledge");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	public static String generateICal(String xmiPath) {
		try {
			return exportModel("platform:/plugin/mbsemmel.m2t/icalEvents.egl", xmiPath,"calendar");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	public static String generateToDo(String xmiPath) {
		try {
			return exportModel("platform:/plugin/mbsemmel.m2t/know2todo.egl", xmiPath, "knowledge");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	  

	  
} 

