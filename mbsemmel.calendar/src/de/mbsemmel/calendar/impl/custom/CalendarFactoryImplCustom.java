package de.mbsemmel.calendar.impl.custom;

import java.time.Period;
import java.time.Duration;
import java.time.ZonedDateTime;

import org.eclipse.emf.ecore.EDataType;

import de.mbsemmel.calendar.impl.CalendarFactoryImpl;

public class CalendarFactoryImplCustom extends CalendarFactoryImpl {
	@Override
	public ZonedDateTime createZonedDateTimeFromString(EDataType eDataType, String initialValue) {
		return ZonedDateTime.parse(initialValue);
	}
	
	@Override
	public Period createPeriodFromString(EDataType eDataType, String initialValue) {
		return Period.parse(initialValue);
	}
	
	@Override
	public Duration createDurationFromString(EDataType eDataType, String initialValue) {
		return Duration.parse(initialValue);
	}
}
